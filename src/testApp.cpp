#include "testApp.h"

#include "ofxZmq.h"

ofxZmqSubscriber subscriber;
ofxZmqPublisher publisher;

//--------------------------------------------------------------
void testApp::setup()
{
	// load resolution and data type
	std::ifstream ifs("../src/zedconfig.txt");
	std::string str;
	for (int i = 0; i < 4; i++) {
		getline(ifs, str);
		if (i == 1) {
			if (str == "VGA") zedresolution_mode = sl::zed::ZEDResolution_mode::VGA;
			else if (str == "HD720") zedresolution_mode = sl::zed::ZEDResolution_mode::HD720;
			else if (str == "HD1080") zedresolution_mode = sl::zed::ZEDResolution_mode::HD1080;
			else if (str == "HD2K") zedresolution_mode = sl::zed::ZEDResolution_mode::HD2K;
			else {
				std::cout << "ZED SIZE in zedconfig.txt is incorrect" << std::endl;
			}
		}
		else if (i == 3) {
			if (str == "uchar") zeddatatype = ZedDataType::UCHAR;
			else if (str == "float") zeddatatype = ZedDataType::FLOAT;
		}
	}
	zed.init(zedresolution_mode, true, true);

	// save intrinsic parameters
	sl::zed::CamParameters campara = zed.getIntrinsicParameters();
	std::ofstream ofs("../src/intrinsicparameters.txt");
	ofs << "fx:" << endl;
	ofs << campara.fx << endl;
	ofs << "fy:" << endl;
	ofs << campara.fy << endl;
	ofs << "cx:" << endl;
	ofs << campara.cx << endl;
	ofs << "cy:" << endl;
	ofs << campara.cy << endl;
	ofs << "k1:" << endl;
	ofs << campara.disto[0] << endl;
	ofs << "k2:" << endl;
	ofs << campara.disto[1] << endl;
	ofs << "k3:" << endl;
	ofs << campara.disto[2] << endl;
	ofs << "r1:" << endl;
	ofs << campara.disto[3] << endl;
	ofs << "r2:" << endl;
	ofs << campara.disto[4] << endl;

	// start server
	publisher.bind("tcp://*:9999");
}

//--------------------------------------------------------------
void testApp::update()
{
	zed.update();

	if (isSending) {
		if (zeddatatype == ZedDataType::UCHAR) {
			if (publisher.send(zed.getDepthBuffer(), (int)zed.getImageDimensions().x * (int)zed.getImageDimensions().y * sizeof(uchar)))
			{
				//cout << "send uchar " + to_string(count) << endl;
				count++;
			}
		}
		else if (zeddatatype == ZedDataType::FLOAT) {
			if (publisher.send(zed.getRawDepthBuffer(), (int)zed.getImageDimensions().x * (int)zed.getImageDimensions().y * sizeof(float)))
			{
				//cout << "send float" + to_string(count) << endl;
				count++;
			}
		}

	}
	else {
		//cout << "isSending == flase" << endl;
	}

	//cout << "update fps: " << to_string(ofGetFrameRate()) << endl;	
	
}

//--------------------------------------------------------------
void testApp::draw()
{
	zed.getDepthTexture()->draw(0, 0);
	ofDrawBitmapString("fps: " + ofToString(ofGetFrameRate()), 20, 20);
}

//--------------------------------------------------------------
void testApp::keyPressed(int key)
{
	if (key == (int)'s') { // press 's' after running osx app
		isSending = !isSending;
	}
}

//--------------------------------------------------------------
void testApp::keyReleased(int key)
{

}

//--------------------------------------------------------------
void testApp::mouseMoved(int x, int y)
{

}

//--------------------------------------------------------------
void testApp::mouseDragged(int x, int y, int button)
{

}

//--------------------------------------------------------------
void testApp::mousePressed(int x, int y, int button)
{

}

//--------------------------------------------------------------
void testApp::mouseReleased(int x, int y, int button)
{

}

//--------------------------------------------------------------
void testApp::windowResized(int w, int h)
{

}

//--------------------------------------------------------------
void testApp::gotMessage(ofMessage msg)
{

}

//--------------------------------------------------------------
void testApp::dragEvent(ofDragInfo dragInfo)
{

}
