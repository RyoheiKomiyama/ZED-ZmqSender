#pragma once

#include "ofMain.h"
#include "ofxZED.h"

class testApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();

		void keyPressed  (int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);

		enum ZedDataType {
			UCHAR,
			FLOAT,
		};

		ofxZED zed;

		// override in setup function
		sl::zed::ZEDResolution_mode zedresolution_mode = sl::zed::ZEDResolution_mode::HD720;
		ZedDataType zeddatatype = ZedDataType::UCHAR;
		
		int count = 0;
		bool isSending = true;
};
