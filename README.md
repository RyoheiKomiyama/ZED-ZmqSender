## ZED-ZmqSender
Sending Depth Data of StereoLabs's ZED Camera.
You can use this with ZED-ZmqReceiver(https://gitlab.com/RyoheiKomiyama/ZED-ZmqSender) implemented with openframeworks.

## Just Run the App
What you only have to do is running the app.
Before running the app, you can change src/zedconfig.txt and choose image size and data type.
After running the app, intrinsicparameters.txt is automatically updated.
You should copy these two files to ZED-ZmqReciver's src directory.